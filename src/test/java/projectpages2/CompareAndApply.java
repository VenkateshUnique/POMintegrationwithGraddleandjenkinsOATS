package projectpages2;

import javax.swing.plaf.multi.MultiButtonUI;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import wdMethods.PreandPost;


public class CompareAndApply extends PreandPost
{	
	public CompareAndApply() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//a[text()='INVESTMENTS']")
	WebElement clickele;
	public MutualFund hovermouse(){
		//WebElement clickele = locateElement("xpath","//a[text()='INVESTMENTS']");
		Actions build = new Actions(driver);
		build.moveToElement(clickele).pause(2000).perform();
		WebElement ele2 = locateElement("xpath", "//a[text()='Mutual Funds']");
		click(ele2);
		return new MutualFund();
		
	}
}
