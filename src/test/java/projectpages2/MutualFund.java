package projectpages2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.PreandPost;

public class MutualFund extends PreandPost
{
	String age = "8 Jun 1993";
	//String salery ="3,00,000";
	public MutualFund clicksearchMF()
	{
		WebElement clickelement = locateElement("linktext", "Search for Mutual Funds");
		click(clickelement);
		return this;
	}
	
	
	public MutualFund selectage()
	{
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement drag = locateElement("class", "rangeslider__handle");
		Actions build = new Actions(driver);
		build.dragAndDropBy(drag, (25-18)*8,0).perform();
		WebElement monthyear = locateElement("linktext","Jun 1993");
		click(monthyear);
		WebElement date = locateElement("xpath", "//div[@aria-label='day-8']");
		click(date);
		return this;
	}
	
	public MutualFund verifyage()
	{
		
		WebElement readdata = locateElement("xpath", "//span[text()='8 Jun 1993']");
		String result = readdata.getText();
		System.out.println(result);
		/*if(result == age) {
			System.out.println("Age is correct and verified");
		}
		else
			System.out.println("Invalid Age selected");*/
		return this;
	}
	
	public MutualFund clicknext() {
		WebElement nextbtn = locateElement("linktext","Continue");
		click(nextbtn);
		return this;
	}
	
	public MutualFund selectsalaryslide() throws InterruptedException
	{
		Thread.sleep(2000);
		Actions build = new Actions(driver);
		WebElement dragsrc = locateElement("xpath", "//div[@class='rangeslider__handle']");
		build.dragAndDropBy(dragsrc,150, 0).perform(); // 8 pixels for every age

		WebElement nextclick = locateElement("linktext", "Continue");
		click(nextclick);
		Thread.sleep(2000);
		return this;
	}
	public MutualFund clickBank() throws InterruptedException {
		WebElement bank = locateElement("xpath", "//span[text()='SBI']");
		click(bank);
		Thread.sleep(2000);
		return this;
	}
	
	public MutualFund enterName()
	{
		WebElement nme = locateElement("name","firstName");
		type(nme, "Venkatara");
		return this;
	}
	
	public InvestinYour clickviewfunds() throws InterruptedException
	{
		WebElement viewbtn = locateElement("linktext","View Mutual Funds");
		click(viewbtn);
		Thread.sleep(5000);
		return new InvestinYour();
	}
	
	
}
