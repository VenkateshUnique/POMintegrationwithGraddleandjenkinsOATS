Feature: feature to create new lead in leaf taps application

Scenario: Create a new lead positive testing

Given Launch the browser and load leaftaps
And Login as the DemoSalesManager credentials DemoSalesManager and crmsfa
And Click on crmsfa link
And Click on Leads Tab
And Click on Create Lead
And Enter the Company Name Cognizant 
And Enter the First name Aravinth 
And Enter the Last name Dhuraisamy
When Click on the Create lead button 
Then Verify the Lead record is created in the page View Lead  

Scenario: Create a new lead negative testing

Given Launch the browser and load leaftaps
And Login as the DemoSalesManager credentials DemoSalesManager and crms
And Click on crmsfa link
And Click on Leads Tab
And Click on Create Lead
And Enter the Company Name Cognizant  
#And Enter the First name 
And Enter the Last name Dhuraisamy  
When Click on the Create lead button 
But Verify the Lead record is created in the page Create Lead
