Feature: Feature to create new lead in leaf taps application

Background:
Given Launch the browser and load leaftaps
And Login as the DemoSalesManager credentials DemoSalesManager and crmsfa

Scenario Outline: Create a new lead positive testing

And Click on crmsfa link
And Click on Leads Tab
And Click on Create Lead
And Enter the Company Name <CompanyName>
And Enter the First name <FirstName> 
And Enter the Last name <LastName>
When Click on the Create lead button 
Then Verify the Lead record is created in the page View Lead  

Examples: 
|CompanyName|FirstName|LastName|
|Zoho corp|Siddharth|Sunderjan|
|ADP India|Venkatesh|Kanagaraj|


Scenario Outline: Create a new lead Negative testing

And Click on crmsfa link
And Click on Leads Tab
And Click on Create Lead
And Enter the Company Name <CompanyName>
And Enter the First name <FirstName> 
And Enter the Last name <LastName>
When Click on the Create lead button 
But Verify the error message <Error>

Examples: 
|CompanyName|FirstName|LastName|Error|
|Zoho corp||Sunderjan|The following required parameter is missing: [crmsfa.createLead.firstName]|



