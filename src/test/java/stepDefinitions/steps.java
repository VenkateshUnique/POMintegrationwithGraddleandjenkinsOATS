package stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class steps 
{
	
	public ChromeDriver driver;
	
	@Given ("Launch the browser and load leaftaps")
	public void launchBrowser()
	{
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps/control/main");
	}
	
	@And ("Login as the DemoSalesManager credentials (.*) and (.*)")
	public void userlogin(String uname,String pwd)
	{
		driver.findElementById("username").sendKeys(uname);
		driver.findElementById("password").sendKeys(pwd);
		driver.findElementByClassName("decorativeSubmit").click();
	}
	
	@And ("Loginnumber as the DemoSalesManager credentials (.*) and (.*)")
	public void userlogin(String uname,int number)
	{
		driver.findElementById("username").sendKeys(uname);
		driver.findElementById("password").sendKeys();
		driver.findElementByClassName("decorativeSubmit").click();
	}
	
	@And ("Click on crmsfa link")
	public void clickcrmsfa() throws InterruptedException
	{
		driver.findElementByLinkText("CRM/SFA").click();
	}
	@And ("Click on Leads Tab")
	public void clickLeadtab()
	{
		driver.findElementByLinkText("Leads").click();
	}
	@And ("Click on Create Lead")
	public void clickcreatelead()
	{
		driver.findElementByLinkText("Create Lead").click();
	}
	@And ("Enter the Company Name (.*)")
	 public void enterDetail(String cName)
	 {
		 driver.findElementById("createLeadForm_companyName").sendKeys(cName);
		 
	 }
	@And ("Enter the First name (.*)")
	public void enterDetail1(String fName)
	 {
		 driver.findElementById("createLeadForm_firstName").sendKeys(fName);
		
	 }
	@And ("Enter the Last name (.*)")
	public void enterDetail2(String lName)
	 {
		 
		 driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	 }
	@When ("Click on the Create lead button")
	public void clickSubmt()
	{
		driver.findElementByClassName("smallSubmit").click();
	}
	@Then ("Verify the Lead record is created in the page (.*)")
	public void verify(String pagetitle)
	{
		String title = driver.getTitle();
		if(title.contains(pagetitle))
		{
			System.out.println("Lead record created "+ title);
		}
		else
		{
			System.out.println("Lead record not created ");
		}
	}
	@But ("Verify the error message (.*)")
	public void verifyError()
	{
		System.out.println("Error message Matched");
	}
	
}
