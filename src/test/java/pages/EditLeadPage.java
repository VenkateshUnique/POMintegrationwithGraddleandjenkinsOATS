package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{
	
	public EditLeadPage changeCompany(String data)
	{
		WebElement eleChangecompany = locateElement("id", "updateLeadForm_companyName");
		type(eleChangecompany, data);
		return this;
	}
	
	public ViewLeadPage clickUpdate()
	{
		WebElement eleClickupdate = locateElement("xpath", "//input[@name='submitButton']");
		click(eleClickupdate);
		return new ViewLeadPage();
	}

}
