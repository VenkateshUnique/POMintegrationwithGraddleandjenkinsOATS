package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC003_DeleteLead extends ProjectMethods {
	@BeforeClass
	public void setData() {
		testCaseName = "TC003_DeleteLead";
		testCaseDescription ="Delete a lead";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC003";
	}
	@Test(dataProvider="fetchData")
	public  void deleteLead(String email, String errorMsg) throws InterruptedException   {
		
		new MyHomePage()
		.clickLeads()
		.clickfindLead()
		.clickEmailTab()
		.typeEmail(email)
		.clickFindleads()
		.clickLeadId()
		.clickDelete()
		.clickfindLead()
		.typeLeadId()
		.clickFindleads()
		.verifyErrorMessage(errorMsg);
	
	}

}
