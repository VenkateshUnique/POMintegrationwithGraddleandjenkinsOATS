package projectpagestestcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import projectpages2.CompareAndApply;
import wdMethods.PreandPost;

public class TC001_BankBazzar extends PreandPost{
	
	@BeforeClass
	public void setData() {
		testCaseName = "Bankazaar";
		testCaseDescription ="Bankazaar";
		category = "Smoke";
		author= "Venkat";
	}
	
	@Test
	public  void viewMutualFunds() throws InterruptedException
	{
		new CompareAndApply()
		.hovermouse()
		.clicksearchMF()
		.selectage()
		.verifyage()
		.clicknext()
		.selectsalaryslide()
		.clickBank()
		.enterName()
		.clickviewfunds();
	}
}
